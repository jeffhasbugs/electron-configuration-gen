#include <iostream>
#include <vector>
#include <memory>
#include <algorithm>

// This program took 1 hour to write :^)
// Mostly because I skipped the required thinking (bad idea)
// Next up: optimization

int main() {
    // describes numbering of subshells 
    const std::vector<int> numbering = { 1, 2, 3, 4, 4, 3, 2 };
    const std::string subshells = "spdf";
    
    for (int i = 0; i < numbering.size(); ++i) {
        int n = numbering[i];
        int s = (i == 0 ? 0 : numbering[i-1] - 1);
        for (int j = s; j < n; ++j) {
            int a = i+1, b = j;
            while (b >= 0) {
                std::cout << a << subshells[b] << " ";
                --b; ++a;
            }
        }
    }
    std::cout << std::endl;
}
